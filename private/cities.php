<?php
require_once('connect.php');
$index = 1;
$sql = "SELECT cities.id, cities.city_name, counties.name FROM cities JOIN counties ON counties.id = cities.county_id WHERE cities.deleted=0";
$result = mysqli_query($conn, $sql);

echo "<table class='table'>
<thead>
    <tr>
      <th scope='col'>#</th>
      <th scope='col' class='city'>City</th>
      <th scope='col' class='county'>County</th>
    </tr>
</thead>
<tbody>";
while($row = mysqli_fetch_array($result))
{
echo "<tr>";
echo "<td id='index'>" . $index . "</td>";
echo "<td><a data-bs-toggle='collapse' href='#" . $row['city_name'] . "' role='button' aria-expanded='false' aria-controls='" . $row['city_name'] . "'>" . $row['city_name'] . "&#9998;</a></td>";
echo "<td>" . $row['name'] . "</td>";
echo "</tr>";
echo "<tr class='collapse' id='". $row['city_name'] ."'>
  <td colspan='3' style='white-space: nowrap'><div class='container kozep'>
  <label for='varosnev' class='form-label citnam'>City name</label>
  <input class='form-control widehardo' id='". $row['city_name'] ."' value='" . $row['city_name'] . "'>
  <button type='button' class='update btn btn-success widehardo' id='". $row['city_name'] ."'>Mentés</button>
  <button type='button' class='delete deleted btn btn-danger widehardo' id='". $row['city_name'] ."'>Város törlése</button>
  </div></td>
</tr>";
$index++;
}
echo "</tbody>";
echo "</table>";

?>
