<?php
require_once('connect.php');
$sql = "SELECT * FROM counties";
$result = mysqli_query($conn, $sql);

echo "<table class='table'>
<thead>
    <tr>
      <th scope='col'>#</th>
      <th scope='col' class='county'>County</th>
    </tr>
</thead>
<tbody>";

while($row = mysqli_fetch_array($result))
{
echo "<tr>";
echo "<td>" . $row['id'] . "</td>";
echo "<td>" . $row['name'] . "</td>";
echo "</tr>";
}

echo "</tbody>";
echo "</table>";

?>
