<?php
require_once('connect.php');
$index = 1;
$sql = "SELECT cities.id, cities.city_name, counties.name FROM cities JOIN counties ON counties.id = cities.county_id WHERE cities.deleted=1";
$result = mysqli_query($conn, $sql);

echo "<table class='table'>
<thead>
    <tr>
      <th scope='col'>#</th>
      <th scope='col' class='city'>City</th>
      <th scope='col' class='county'>County</th>
    </tr>
</thead>
<tbody>";
while($row = mysqli_fetch_array($result))
{
echo "<tr>";
echo "<td id='index'>" . $index . "</td>";
echo "<td>". $row['city_name'] . "</td>";
echo "<td>" . $row['name'] . "</td>";
echo "<td><a class='restore deleted' id='". $row['city_name'] ."'>Restore</a></td>";
echo "</tr>";
$index++;
}
echo "</tbody>";
echo "</table>";

?>
