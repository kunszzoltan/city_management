$(".add").click(function(){
      var id = $("input").val();
      var name = $("select").val();
      console.log(id);
      console.log(name);
      $.ajax({
          type:"POST",
          url: "private/add.php",
          dataType: "HTML",
          data: {data:id, name:name},
          success : function (data) {
              $("#content").empty();
              document.getElementById("content").innerHTML = "<div class='container text-center'><h1>New city added!</h1></div>";
              $.getScript('js/language.js');
      },
      error: function (jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
        $('#content').html(msg);
    }
  })
})
