var userLang = navigator.language || navigator.userLanguage;
$.ajax({
  type: 'GET',
  url: './languages/' + userLang.substring(0,2) + '.json',
  dataType: 'JSON',
  data: JSON.stringify({}),
  success: function (data) {
    console.log(JSON.stringify(data));
    $(".couies").text(data.couies);
    $(".county").text(data.county);
    $(".cities").text(data.cities);
    $(".city").text(data.city);
    $(".delcit").text(data.delcit);
    $(".newcit").text(data.newcit);
    $(".citnam").text(data.citnam);
    $(".restore").text(data.resore);
    $(".update").text(data.update);
    $(".delete").text(data.delete);
    $(".add").text(data.add);
  },
  error: function (jqXHR, exception) {
    var msg = '';
    if (jqXHR.status === 0) {
        msg = 'Not connect.\n Verify Network.';
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
        this.url='./languages/en.json'
        $.ajax(this);
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        msg = 'Time out error.';
    } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Uncaught Error.\n' + jqXHR.responseText;
    }
    $('#content').html(msg);
}
  });
