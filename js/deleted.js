$(".deleted").click(function(){
      var id = $(this).attr('id');
      console.log($(this).attr('id'));
      var str = $(this).attr('class');
      var arr = str.split(" ");
      console.log(arr);
      $.ajax({
          type:"POST",
          url: "private/"+arr[0]+".php",
          dataType: "HTML",
          data: {data:id},
          success : function (data) {
              $("#content").empty();
              document.getElementById("content").innerHTML = "<div class='container text-center sucupd'><h1>Successfull "+arr[0]+"!</h1></div>";
              $.getScript('js/language.js');
      },
      error: function (jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
        $('#content').html(msg);
    }
  })
})
