  $("a").click(function(){
        $.ajax({
            type:"POST",
            url: "private/"+$(this).attr('value')+".php",
            dataType: "HTML",
            success : function (data) {
                $("#content").empty();
                $("#content").html(data);
                $.getScript('js/language.js');
                $.getScript('js/deleted.js');
                $.getScript('js/update.js');
                $.getScript('js/add_new_city.js');
        },
        error: function (jqXHR, exception) {
          var msg = '';
          if (jqXHR.status === 0) {
              msg = 'Not connect.\n Verify Network.';
          } else if (jqXHR.status == 404) {
              msg = 'Requested page not found. [404]';
          } else if (jqXHR.status == 500) {
              msg = 'Internal Server Error [500].';
          } else if (exception === 'parsererror') {
              msg = 'Requested JSON parse failed.';
          } else if (exception === 'timeout') {
              msg = 'Time out error.';
          } else if (exception === 'abort') {
              msg = 'Ajax request aborted.';
          } else {
              msg = 'Uncaught Error.\n' + jqXHR.responseText;
          }
          $('#content').html(msg);
      }
    })
  })
